import './App.scss';
  // import Dashboard from './modules/dashboard/dashboard';
  // import Home from './modules/home/Home';
import { BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";
// import Header from './components/header/app_header';

// import Aboutus from './modules/about_us/aboutus';
// import Blog from './modules/blog/final';
// import Blogform from './modules/addblogpage/blogform';

// import Footer from './components/footer/footer';
// import Butcherypopup from './modules/butchery_form_page/form2';

import LandingPage from './modules/landing_page/LandingPage';
import Header from './modules/shopping_cart/cartpopup/popup';
import Shoppingcart from "./modules/shopping_cart/Shoppingcart";
import Shoppingproduct from './modules/shopping_cart/Shoppingproduct';

function App() {
  return (
    <div className="App">

      <Router>
        <div>
          
          <Routes>
            {/* <Route path="/" element={<Shoppingproduct/>}>
            </Route> */}
            
            <Route path="/" element={<Header />}></Route>
            <Route path ="/shoppingcart" element={<Shoppingcart/>}></Route>
            
          </Routes>
         
        </div>
      </Router>
      {/* <Router>
        <div>
          <Header/>
          <Routes>
            <Route path="/" element={<LandingPage/>}>
            </Route>
            <Route path="/aboutus" element={<Aboutus/>}>
            </Route>
            <Route path="/blog" element={<Blog/>}>
            </Route>
            <Route path="/addblogs" element={<Blogform />}>
            </Route>
          </Routes>
          <Footer/>
        </div>
      </Router> */}
      {/* <Butcherypopup></Butcherypopup> */}
    </div>
  );
}


export default App;
